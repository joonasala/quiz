import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-results',
  templateUrl: './results.page.html',
  styleUrls: ['./results.page.scss'],
})
export class ResultsPage implements OnInit {

  duration: number;
  durationSeconds: number;
  correctOptions: number;
  nbrOfQuestions: number;
  correctness: number;
  feedback: string;

  constructor(public router: Router, public activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    this.duration = Number(this.activatedRoute.snapshot.paramMap.get('duration'));
    this.durationSeconds = Math.round((this.duration) / 1000);
    this.correctOptions = Number(this.activatedRoute.snapshot.paramMap.get('corAns'));
    this.nbrOfQuestions = Number(this.activatedRoute.snapshot.paramMap.get('qstAmount'));
    this.correctness = this.correctOptions / this.nbrOfQuestions * 100;
    this.result();
  }
  result() {
    if (this.durationSeconds < 15 && this.correctOptions === 4) {
          this.feedback =  'Top performance and what a speed! You are an true athlete!';

    } else if (this.durationSeconds > 16 && this.correctOptions === 4) {
          this.feedback = 'All right, clear game!';

    } else if (this.correctOptions === 3) {
      this.feedback = 'Well done, you have been in stadium sometimes!';

    } else if (this.correctOptions < 3) {
      this.feedback = 'Well, not so interested about sports?';
    }
  }
    correctAnswers() {
      this.router.navigateByUrl('answers');
  }
}
