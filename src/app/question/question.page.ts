import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { Questions } from '../../questions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit {

  questions: Questions[] = [];
  activeQuestion: Questions;
  feedback: string;
  questionCounter: number;
  rightCounter: number;

  startTime: Date;
  endTime: Date;
  duration: number;

  constructor(public router: Router) {}


  ngOnInit() {
    fetch('../../assets/data/questions.json').then(res => res.json())
    .then(json => {
      this.questions = json;
      this.questionCounter = 0;
      this.rightCounter = 0;
      this.setQuestion();
      this.startTime = new Date();
      
    });
  }

  setQuestion() {
    if (this.questionCounter === this.questions.length) {
      this.endTime = new Date();
      this.duration = this.endTime.getTime() - this.startTime.getTime();
      this.router.navigateByUrl('results/' + this.duration + '/' + this.rightCounter + '/' + this.questions.length);
      
    } else {
      this.feedback = '';
      this.activeQuestion = this.questions[this.questionCounter];
      this.questionCounter++;
    }
    
  }
  checkOption(option: number, activeQuestion: Questions) {
    if (option === activeQuestion.correctOption) {
      this.rightCounter++;
      this.feedback = activeQuestion.options[option] +
        ' is correct! Tap the arrow to continue!';
        
    } else {
      this.feedback = 'Wrong answer, better luck next time! Tap the arrow to continue.';
      
    }
  }
}