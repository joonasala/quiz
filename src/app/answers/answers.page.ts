import { Component, OnInit } from '@angular/core';
import { Questions } from '../../questions';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.page.html',
  styleUrls: ['./answers.page.scss'],
})
export class AnswersPage implements OnInit {

  questions: Questions[] = [];

  constructor(public router: Router, public activatedRoute: ActivatedRoute) {
    
  }
  
  ngOnInit() {
      fetch('../../assets/data/questions.json').then(res => res.json())
      .then(json => {
        this.questions = json;
   });
  }
  goHome() {
      this.router.navigateByUrl('home');
  }
}


