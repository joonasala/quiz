export class Questions {
    question: string;
    options: string[];
    correctOption: number;
}